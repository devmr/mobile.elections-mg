
import 'dart:async';
import 'dart:convert';

import 'package:electionsflutter/services/api.dart';
import 'package:electionsflutter/services/utils.dart';
import 'package:flutter/material.dart';
import 'package:electionsflutter/models/candidat.dart';
import 'package:http/http.dart' as http;

class SearchCandidatPage extends StatefulWidget {
  @override
  State createState() => new SearchList();
}

class Debouncer {
  final int milliseconds;
  VoidCallback action;
  Timer _timer;

  Debouncer({this.milliseconds});

  run(VoidCallback action) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}

class SearchList extends State<SearchCandidatPage> {

  final _debouncer = Debouncer(milliseconds: 500);

  List<String> items = [];
  Future<List<Candidat>> candidatServ ;
  List candidats = [];
  FocusNode myFocusNode;
  TextEditingController txtCtrl;
  bool isSearch = false;


  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: Text("Recherche"),
    ),
    body: Container(

      padding: const EdgeInsets.all(10.0),
    child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          TextField(
              focusNode: myFocusNode,
              controller: txtCtrl,
              autofocus: true,
              decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search),
                  labelText: 'Chercher un candidat...'
              ),
              onChanged: (text) {

                //_debouncer.run(() {
                  print("First text field: $text - isempty: " + text.isEmpty.toString());
                  print("candidats.length: " + candidats.length.toString());

                  if (!text.isEmpty && candidats.length > 0) {
                    candidats.length = 0;
                  }


                  if (!text.isEmpty) {
            //                  this.searchRemote(text);

                    candidatServ = Api.searchCandidat(text);
                    candidatServ.then((rows) {
            //                    print('candidatsRows: ${rows.length}');
            //                    print('name0: ${rows[0].name}');

                      setState(() {
                        candidats = rows;
                        isSearch = true;
                      });
                    });
                  }
                //});

              }
          ),
          Visibility (
            visible: isSearch,
            child: LinearProgressIndicator()
          ),
          Expanded(
              child: new ListView.builder
                (
                  itemCount: candidats.length,
                  itemBuilder: (context, index) {

                    var u = new Util();

                    int idC = candidats[index].id;
                    String nameC = u.getStringNotnullValue(candidats[index].name, "");
                    String name2C = (nameC!='' ? ' ' : '') + u.getStringNotnullValue(candidats[index].name2, "");
                    String firstnameC = (name2C!='' ? ' ' : '') + u.getStringNotnullValue(candidats[index].firstname, "");
                    String firstname2C = (firstnameC!='' ? ' ' : '') + u.getStringNotnullValue(candidats[index].firstname2, "");

                    return ListTile(
                      leading: Icon(Icons.search),
                      title: Text(nameC+name2C+firstnameC+firstname2C),
                      onTap: () {
                        // do something
                        Navigator.pop(context, candidats[index]);
                      },
                    );
                  }
              )
          )
        ]
    )
    )
    );
  }


}
