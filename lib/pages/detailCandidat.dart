
import 'dart:convert';

import 'package:electionsflutter/models/CandidatElection.dart';
import 'package:electionsflutter/services/api.dart';
import 'package:electionsflutter/services/utils.dart';
import 'package:flutter/material.dart';
import 'package:electionsflutter/models/candidat.dart';
import 'package:electionsflutter/models/sexe.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:truncate/truncate.dart';

import 'CandidatElectionEditPage.dart';

class DetailCandidatPage extends StatefulWidget {

  final candidat;

  DetailCandidatPage({Key key, this.candidat}) : super(key: key);

  @override
  DetailCandidat createState() => DetailCandidat();
}

var u = new Util();

class DetailCandidat extends State<DetailCandidatPage> {


  String _ddmmYYYY = "";
  String birthday = "";

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentCity;

  String _mySelection;

  String candidatFullName = "";

  List<CandidatElection> _employees = [];

  List<Sexe> data = []; //edited line

  getSexeCandidat() {

    String sexeCandidat = "1";
    if (widget.candidat.sexe != null) {
      sexeCandidat = u.getStringNotnullValue(widget.candidat.sexe.id.toString(), "2");
       print("sexe: " + widget.candidat.sexe.id.toString());
    }
    var services = Api.getAllSexe();
    services.then((rows) {
      setState(() {
        data = rows;
        _mySelection = sexeCandidat;
      });
    });

  }

  @override
  void initState() {
    super.initState();
//    this.getSexeData();
    this.getSexeCandidat();
  }

  SingleChildScrollView _dataBody() {

    print("candidat_election: " + widget.candidat.candidat_election.length.toString() );


    setState(() {
      _employees = widget.candidat.candidat_election;
    });

    // Both Vertical and Horozontal Scrollview for the DataTable to
    // scroll both Vertical and Horizontal...
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
          columns: [
            DataColumn(
              label: Text('Election'),
            ),
            DataColumn(
              label: Text('Numero'),
            ),
            DataColumn(
              label: Text('--'),
            )

          ],
          rows: _employees.map(( employee) => DataRow(cells: [
              DataCell(
                Text( truncate(u.getStringNotnullValue(employee.electionid.name, "--"), 20, omission: "...", position: TruncatePosition.end))
              ),
              DataCell(
                  Text(u.getStringNotnullValue(employee.numero_bulletin.toString(), "--"))
              ),
              DataCell(IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
//                  _deleteEmployee(employee);
                  openPageCandidatElectionEdit(employee);
                },
              ))
          ])).toList(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
//    print(widget.candidat["sexe"]["id"].toString());

    var u = new Util();
    String nameC = u.getStringNotnullValue(widget.candidat.name, "");
    birthday = u.getStringNotnullValue(widget.candidat.birthdate, "");
    _ddmmYYYY = u.getStringDatePart(birthday, "d") + "-" + u.getStringDatePart(birthday,"m") + "-" + u.getStringDatePart(birthday,"y");
    int childrenCount = u.getIntNotnullValue(widget.candidat.children_count, 0);
    String name2C = (nameC !='' ? ' ' : '') + u.getStringNotnullValue(widget.candidat.name2, "");
    String firstnameC = (name2C !='' ? ' ' : '') + u.getStringNotnullValue(widget.candidat.firstname, "");
    String firstname2C = (firstnameC !='' ? ' ' : '') + u.getStringNotnullValue(widget.candidat.firstname2, "");
    String names = nameC + name2C + firstnameC + firstname2C;

    setState(() {
      candidatFullName = names;
    });
    return Scaffold(
        appBar: AppBar(
        title: Text(names),
    ),
    body: Container(

    padding: const EdgeInsets.all(10.0),
    child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new TextFormField(
                initialValue: nameC,
                decoration: new InputDecoration(
                    hintText: 'Prénom 1',
                    labelText: 'Prénom 1'
                )
            ),
            new TextFormField(
                initialValue: name2C,
                decoration: new InputDecoration(
                    hintText: 'Prénom 2',
                    labelText: 'Prénom 2'
                )
            ),
            new TextFormField(
                initialValue: firstnameC,
                decoration: new InputDecoration(
                    hintText: 'Nom 1',
                    labelText: 'Nom 1'
                )
            ),
            new TextFormField(
                initialValue: firstname2C,
                decoration: new InputDecoration(
                    hintText: 'Nom 2',
                    labelText: 'Nom 2'
                )
            ),
            new FlatButton(
              onPressed: () {

                print('_ddmmYYYY before $_ddmmYYYY');
//                print('candidat.birthdate: ' + u.getStringNotnullValue(candidat["birthdate"], ""));


                DatePicker.showDatePicker(context,
                    showTitleActions: true,
                    minTime: DateTime(1900, 1, 1),
                    maxTime: DateTime.now(),
                    theme: DatePickerTheme(
                        backgroundColor: Colors.red,
                        itemStyle: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                        doneStyle:
                        TextStyle(color: Colors.white, fontSize: 16)),
                    onChanged: (date) {
                      print('change $date in time zone ' +
                          date.timeZoneOffset.inHours.toString());
                    }, onConfirm: (date) {

                      print('confirm $date');
                      birthday = date.toString().replaceAll(" ", "T").replaceAll("\.000", "+00:00");


                      setState(() {
                        print('birthday $birthday');
//                        candidat["birthdate"] = birthday.toString();
                      });
                    },
//                    currentTime: DateTime.now(), locale: LocaleType.fr);
                    currentTime: (birthday!="" ? DateTime(u.getDatePart(birthday,"y"), u.getDatePart(birthday,"m"), u.getDatePart(birthday,"d") ) : DateTime.now()), locale: LocaleType.fr);
              },
              child: Text(
                "Date de naissance : " + _ddmmYYYY,
                textAlign: TextAlign.left,
              )
            ),
            new TextFormField(
                keyboardType: TextInputType.number,
                initialValue: childrenCount.toString(),
                decoration: new InputDecoration(
                    hintText: 'Nb Enfants',
                    labelText: 'Nb Enfants'
                )
            ),
            new DropdownButton(
                items: data.map((item) {
                  return new DropdownMenuItem(
                    child: new Text(item.name),
                    value: item.id.toString(),
                  );
                }).toList(),
                onChanged: (newVal) {
                  setState(() {
                    _mySelection = newVal;
                  });
                },
                value: _mySelection,
            ),
            Expanded(
              child: _dataBody(),
            )
          ]

    )
    )
    );
  }
  void changedDropDownItem(String selectedCity) {
    setState(() {
      _currentCity = selectedCity;
    });
  }

  Future openPageCandidatElectionEdit(CandidatElection) async {


    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CandidatElectionEditPage(CandidatElection: CandidatElection, candidatFullName: candidatFullName)),
    );

    // Result

    if (result != "") {
      print("Retour form: " + result);
    }

  }
}
