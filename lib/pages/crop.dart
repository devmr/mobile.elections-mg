import 'dart:io';

import 'package:flutter/material.dart';
import 'package:electionsflutter/models/crop.dart';
import 'package:image_crop/image_crop.dart';

class CropPage extends StatelessWidget {

  // Declare a field that holds the Todo.
  final CropEntity cropEntity;

  final cropKey = GlobalKey<CropState>();
  File _file;
  File _sample;
  File _lastCropped;

  // In the constructor, require a Todo.
  CropPage({Key key, @required this.cropEntity}) : super(key: key);

  Future<void> _cropImage(BuildContext context) async {
    final scale = cropKey.currentState.scale;
    final area = cropKey.currentState.area;
    if (area == null) {
      // cannot crop, widget is not setup
      return null;
    }

    // scale up to use maximum possible number of pixels
    // this will sample image in higher resolution to make cropped image larger
    final sample = await ImageCrop.sampleImage(
      file: _file,
      preferredSize: (2000 / scale).round(),
    );

    final file = await ImageCrop.cropImage(
      file: sample,
      area: area,
    );

//    sample.delete();

    _lastCropped?.delete();
    _lastCropped = file;


    Navigator.pop(context, file.path);

  }

  @override
  Widget build(BuildContext context) {

    _sample = new File(cropEntity.description);
    _file = new File(cropEntity.description);

    return Scaffold(
      appBar: AppBar(
        title: Text(cropEntity.name),
      ),
      body: Container(
          padding: const EdgeInsets.all(10.0),

          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Crop.file(_sample, key: cropKey),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 20.0),
                  alignment: AlignmentDirectional.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      RaisedButton(
                        child: Text(
                            'Appliquer les changements'
                        ),
                        onPressed: () {
                          _cropImage(context);
                        },
                      ),
                      // _buildOpenImage(),
                    ],
                  ),
                ),
              ]
          )
      ),
    );
  }
}
