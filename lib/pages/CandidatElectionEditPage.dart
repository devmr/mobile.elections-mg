
import 'dart:convert';

import 'package:electionsflutter/models/CandidatElection.dart';
import 'package:electionsflutter/services/api.dart';
import 'package:electionsflutter/services/utils.dart';
import 'package:flutter/material.dart';
import 'package:electionsflutter/models/candidat.dart';
import 'package:electionsflutter/models/sexe.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:truncate/truncate.dart';

var u = new Util();

class CandidatElectionEditPage extends StatefulWidget {

  final CandidatElection;
  final candidatFullName;



  CandidatElectionEditPage({Key key, this.CandidatElection, this.candidatFullName}) : super(key: key);

  @override
  CandidatElectionEdit createState() => CandidatElectionEdit();
}



class CandidatElectionEdit extends State<CandidatElectionEditPage> {

  TextEditingController _numeroBulletinController;


  @override
  void initState() {
    super.initState();
    _numeroBulletinController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {

    String ElectionName = u.getStringNotnullValue(widget.CandidatElection.electionid.name, "");
    String CandidatName = u.getStringNotnullValue(widget.candidatFullName, "");
    int numeroElection = u.getIntNotnullValue(widget.CandidatElection.numero_bulletin, 0);

    setState(() {
      _numeroBulletinController.text = numeroElection.toString();
    });

    return Scaffold(
        appBar: AppBar(
        title: Text(ElectionName),
    ),
    body: Container(

    padding: const EdgeInsets.all(10.0),
    child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Text(
                ElectionName
            ),
            SizedBox(
              height: 20.0,
            ),
            new Text(
                CandidatName
            ),
            SizedBox(
              height: 20.0,
            ),
            new TextField(
                controller: _numeroBulletinController,
                keyboardType: TextInputType.number ,
                decoration: new InputDecoration(
                    hintText: 'Numero bulletin',
                    labelText: 'Numero bulletin'
                )
            ),
            SizedBox(
              height: 20.0,
            ),
            new RaisedButton(
                child: Text(
                    'Enregistrer et retour'
                ), onPressed: () {
//                  print("value: " + _numeroBulletinController.text);
                    Navigator.pop(context,  _numeroBulletinController.text);
            },
            )
          ]

    )
    )
    );
  }
}
