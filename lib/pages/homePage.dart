

import 'dart:async';
import 'dart:convert';
import 'dart:io';


import 'package:electionsflutter/pages/crop.dart';
import 'package:electionsflutter/pages/detailCandidat.dart';
import 'package:electionsflutter/pages/searchCandidat.dart';
import 'package:electionsflutter/services/api.dart';
import 'package:electionsflutter/services/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:image_crop/image_crop.dart';

import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';

import 'package:electionsflutter/models/crop.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  StreamSubscription _intentDataStreamSubscription;
  List<SharedMediaFile> _sharedFiles;
  String _sharedText;

  Future<File> file;
  String status = '';
  String base64Image;
  File tmpFile;
  File croppedFileResult;
  String errMessage = 'Error Uploading Image';

  final cropKey = GlobalKey<CropState>();
  File _lastCropped;

  bool _isBtnCropDisabled = true;
  bool _isBtnUploadDisabled = true;
  bool _isShowCandidat = false;

  var candidatIDSelected;

  // Select Image from Gallery/Camera
  chooseImage() {

    croppedFileResult = null;

    setState(() {
      file = ImagePicker.pickImage(source: ImageSource.gallery);
      _sharedFiles = null;

      if (file!=null) {
        _isBtnCropDisabled = false;
        _isBtnUploadDisabled = _isBtnCropDisabled;
      }

    });
  }
  // Upload File
  startUpload() {
    setStatus('Uploading Image...');
    String StrFileShared = (_sharedFiles?.map((f)=> f.path)?.join(",") ?? "");
    String StrFileCropped = (croppedFileResult != null ? croppedFileResult.path : "");
    String StrFileChoosed = (tmpFile != null ? tmpFile.path : "");
    print("StrFileShared :" + StrFileShared);
    print("StrFileChoosed :" + StrFileChoosed);
    print("StrFileCropped :" + StrFileCropped);

    if (StrFileShared == '' && StrFileChoosed == '' && StrFileCropped == '') {
      setStatus(errMessage);
      return;
    }

    if (StrFileCropped != '') {
      tmpFile = croppedFileResult;
      base64Image = base64Encode(croppedFileResult.readAsBytesSync());
    }

    if (null == tmpFile){
      tmpFile = new File(StrFileShared);
      base64Image = base64Encode(tmpFile.readAsBytesSync());
    }

    String fileName = tmpFile.path.split('/').last;

    print("tmpFile.path :" + tmpFile.path);
    print("fileName :" + fileName);

    Future<http.Response> result = upload(fileName, base64Image);

    result.then((result) {
      setStatus(result.statusCode == 200 ? result.body : errMessage);
    }).catchError((error) {
      setStatus(error);
    });
  }

  Widget showImage() {
    return FutureBuilder<File>(
      future: file,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {


        bool isSnapShot = (snapshot.connectionState == ConnectionState.done && null != snapshot.data);

        String StrFileShared = (_sharedFiles?.map((f)=> f.path)?.join(",") ?? "");
        String StrFileCropped = (croppedFileResult != null ? croppedFileResult.path : "");
        String StrFileChoosed = (tmpFile != null ? tmpFile.path : "");
        print("StrFileShared :" + StrFileShared);
        print("StrFileChoosed :" + StrFileChoosed);
        print("StrFileCropped :" + StrFileCropped);

        print("snapshot.connectionState == ConnectionState.done: " + (snapshot.connectionState == ConnectionState.done).toString());
        print("null != snapshot.data: " + (null != snapshot.data).toString());

        if (!isSnapShot && StrFileShared != '') {
          isSnapShot = true;
        }


        if (isSnapShot) {
          _isBtnCropDisabled = false;
          _isBtnUploadDisabled = _isBtnCropDisabled;

          String StrFileCropped = (croppedFileResult != null ? croppedFileResult.path : "");
          bool choosedCroppedSame = (snapshot!=null && snapshot.data == croppedFileResult);

          tmpFile = (snapshot!=null ? snapshot.data : null);

          if ( StrFileShared != '') {
            tmpFile = new File(_sharedFiles?.map((f)=> f.path)?.join(",") ?? "");
          }

          if (!choosedCroppedSame && StrFileCropped != '') {
            tmpFile = croppedFileResult;
          }

          base64Image = base64Encode(tmpFile.readAsBytesSync());

          return Flexible(
            child: Image.file(
              tmpFile,
              fit: BoxFit.fill,
            ),

          );

        } else if (null != snapshot.error) {
          _isBtnCropDisabled = true;
          _isBtnUploadDisabled = _isBtnCropDisabled;

          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          _isBtnCropDisabled = true;
          _isBtnUploadDisabled = _isBtnCropDisabled;
          return const Text(
            'Aucune image choisie',
            textAlign: TextAlign.center,
          );
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();

    // For sharing images coming from outside the app while the app is in the memory
    _intentDataStreamSubscription =
        ReceiveSharingIntent.getMediaStream().listen((List<SharedMediaFile> value) {
          setState(() {
            print("Shared:" + (_sharedFiles?.map((f)=> f.path)?.join(",") ?? ""));
            _sharedFiles = value;
          });
        }, onError: (err) {
          print("getIntentDataStream error: $err");
        });

    // For sharing images coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialMedia().then((List<SharedMediaFile> value) {

      _isBtnCropDisabled = value == null;
      _isBtnUploadDisabled = _isBtnCropDisabled;

      setState(() {
        _sharedFiles = value;

      });
    });

    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    _intentDataStreamSubscription =
        ReceiveSharingIntent.getTextStream().listen((String value) {
          setState(() {
            _sharedText = value;
          });
        }, onError: (err) {
          print("getLinkStream error: $err");
        });

    // For sharing or opening urls/text coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialText().then((String value) {
      setState(() {
        _sharedText = value;
      });
    });
  }

  @override
  void dispose() {
    _intentDataStreamSubscription.cancel();
    super.dispose();
    _lastCropped?.delete();
  }

  setStatus(String message) {
    setState(() {
      status = message;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        padding: EdgeInsets.all(30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            OutlineButton(
              onPressed: chooseImage,
              child: Row( // Replace with a Row for horizontal icon + text
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.file_upload),
                  Text(" Choisir image")
                ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            showImage(),
            SizedBox(
              height: 10.0,
            ),
            OutlineButton(
                onPressed: _isBtnCropDisabled ? null : () async {
                  String desc = (tmpFile!=null ? tmpFile.path : (_sharedFiles != null ? _sharedFiles?.map((f)=> f.path)?.join(",") ?? "" : ""));
                  CropEntity cropEntity = new CropEntity("Rogner image", desc );
                  final result = await Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CropPage(cropEntity: cropEntity)),
                  );

                  croppedFileResult = new File(result);
                  showImage();
                },
                padding: EdgeInsets.all(10.0),
                child: Row( // Replace with a Row for horizontal icon + text
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.crop),
                    Text(" Rogner image")
                  ],
                ),
                color: Colors.grey
            ),
            SizedBox(
              height: 10.0,
            ),
            OutlineButton(
              onPressed: () {
                openPageSearch();
              },
              color: Colors.orange,
              padding: EdgeInsets.all(5.0),
              child: Row( // Replace with a Row for horizontal icon + text
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.search),
                  Text(" Chercher un candidat")
                ],
              ),
            ),
            OutlineButton(
              onPressed: () {
                startUpload();
              },
              color: Colors.orange,
              padding: EdgeInsets.all(5.0),
              child: Row( // Replace with a Row for horizontal icon + text
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.cloud_upload),
                  Text(" Envoyer l'image")
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              status,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.green,
                fontWeight: FontWeight.w500,
                fontSize: 20.0,
              ),

            ),
            Visibility (
                visible: _isShowCandidat,
                child: Column(
                    children: <Widget>[
                      OutlineButton(
                        onPressed: () {
                          openPageCandidat(candidatIDSelected);
                        },
                        color: Colors.orange,
                        padding: EdgeInsets.all(5.0),
                        child: Row( // Replace with a Row for horizontal icon + text
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.edit),
                            Text(" Info et détail candidat")
                          ],
                        ),
                      ),
                    ]
                )
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: chooseImage,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Future openPageSearch() async {


    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SearchCandidatPage()),
    );
//    print("result (L386) : " + result.photo_candidat.toString());


    if (result.name != null) {

      var u = new Util();

      String idC = " (" + result.id.toString() + ")";
      String nameC = u.getStringNotnullValue(result.name, "");
      String name2C = (nameC!='' ? ' ' : '') +  u.getStringNotnullValue(result.name2, "");
      String firstnameC = (name2C!='' ? ' ' : '') + u.getStringNotnullValue(result.firstname, "");
      String firstname2C = (firstnameC!='' ? ' ' : '') + u.getStringNotnullValue(result.firstname2, "");
      setStatus(nameC + name2C + firstnameC + firstname2C + idC );
      setState(() {
        _isShowCandidat = true;
        candidatIDSelected = result;
      });
    }

  }
  Future openPageCandidat(candidat) async {

    print(candidat.toString());


    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DetailCandidatPage(candidat: candidat)),
    );

  }
}
