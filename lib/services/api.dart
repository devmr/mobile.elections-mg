import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:math';
import 'package:electionsflutter/models/candidat.dart';
import 'package:electionsflutter/models/sexe.dart';
import 'package:electionsflutter/services/utils.dart';
import 'package:http/http.dart' as http;

const String uploadEndPoint = 'https://adminer.madatsara.com';

Future<String> api(String orgid) async {
  http.Response response = await http.get(
    Uri.encodeFull(uploadEndPoint),
  );
  return response.body;
}

Future<http.Response> upload(String fileName, String base64Image) async {

  http.Response response = await http.post(uploadEndPoint + '/upload.php', body: {
    "image": base64Image,
    "name": fileName,
  });

  return response;

}
var u = new Util();

class Api {

  // Login
  static Future<bool> login() async {
    // Simulate a future for response after 2 second.
    return await new Future<bool>.delayed(
        new Duration(
            seconds: 2
        ), () => new Random().nextBool()
    );
  }

  static Future<List<Candidat>> searchCandidat(text) async {

    String url = u.getApiUrl() + 'candidat/?name=' + text;
    print(url);

    try {
      final response = await http.get(
          url,
          headers: {"Accept": "application/json"}
      );
      if (response.statusCode == 200) {
        List<Candidat> list = parseCandidats(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<List<Sexe>> getAllSexe() async {

    String url = u.getApiUrl() + "sexe";

    try {
      final response = await http.get(
          url,
          headers: {"Accept": "application/json"}
      );
      if (response.statusCode == 200) {
        List<Sexe> list = parseSexes(response.body);
        return list;
      } else {
        throw Exception("Error");
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<Candidat> parseCandidats(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Candidat>((json) => Candidat.fromJson(json)).toList();
  }

  static List<Sexe> parseSexes(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Sexe>((json) => Sexe.fromJson(json)).toList();
  }

}

