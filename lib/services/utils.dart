
class Util {

  getBaseUrl() {
    return "https://dev.elections-mg.com";
  }

  getApiUrl() {
    return this.getBaseUrl() + "/api/";
  }

  getStringNotnullValue(String s, String defaultValue) {
    return (s != null ? s : defaultValue);
  }

  getIntNotnullValue(int s, int defaultValue) {
    return (s != null ? s : defaultValue);
  }


  int getDatePart(String birthday, String s) {
    var arr = birthday.split("T");
    String ymd = arr[0];
    var arr2 = ymd.split("-");
    String y = arr2[0];
    String m = arr2[1];
    String d = arr2[2];
    switch (s) {
      case "y": {
        return int.parse(y);
      }
      case "m": {
        return int.parse(m);
      }
      case "d": {
        return int.parse(d);
      }
    }
    return 0;
  }

  String getStringDatePart(String birthday, String s) {
    if (birthday == "") {
      return "";
    }
    var arr = birthday.split("T");
    String ymd = arr[0];
    var arr2 = ymd.split("-");
    String y = arr2[0];
    String m = arr2[1];
    String d = arr2[2];
    switch (s) {
      case "y": {
        return y;
      }
      case "m": {
        return m;
      }
      case "d": {
        return d;
      }
    }
    return "0";
  }
}
