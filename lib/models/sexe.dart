

class Sexe {

  int id;
  String name;

  Sexe({
    this.id,
    this.name
  });

  factory Sexe.fromJson(Map<String, dynamic> json) {
    return Sexe(
      id: json['id'],
      name: json['name']
    );
  }
}
