
import 'package:flutter/cupertino.dart';

import 'Election.dart';

class CandidatElection {

  int id;
  int numero_bulletin;
  Election electionid;

  CandidatElection({
    this.id,
    this.numero_bulletin,
    this.electionid
  });

  factory CandidatElection.fromJson(Map<String, dynamic> json) {
    return CandidatElection(
        id: json['id'],
        numero_bulletin: json['numero_bulletin'],
        electionid: Election.fromJson(
          json['electionid'],
        )
    );
  }
}

