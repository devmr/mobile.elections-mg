
import 'package:flutter/cupertino.dart';

class PhotoCandidat {

  String photo;
  bool encours;

  PhotoCandidat(String photo, bool encours ) {
    this.photo = photo;
    this.encours = encours;
  }

  PhotoCandidat.fromJson(Map json) :
        photo = json['photo'],
        encours = json['encours'];

  Map toJson() {
    return {
      'photo' : photo,
      'encours' : encours
    };
  }
}

