
import 'package:electionsflutter/models/photoCandidat.dart';
import 'package:electionsflutter/models/sexe.dart';
import 'package:flutter/cupertino.dart';

import 'CandidatElection.dart';

class Candidat {

  int id;
  String name;
  String name2;
  String firstname;
  String firstname2;
  String birthdate;
  int children_count;
  String slug;
  Sexe sexe;
  List<PhotoCandidat> photo_candidat;
  List<CandidatElection> candidat_election;

  Candidat({
      this.id,
      this.name,
      this.name2,
      this.firstname,
      this.firstname2,
      this.slug,
      this.children_count,
      this.birthdate,
      this.photo_candidat,
      this.candidat_election,
      this.sexe
  });

  factory Candidat.fromJson(Map<String, dynamic> json) {
    return Candidat(
      id : json['id'],
      name : json['name'],
      name2 : json['name2'],
      firstname : json['firstname'],
      firstname2 : json['firstname2'],
      slug : json['slug'],
      children_count : json['children_count'],
      birthdate : json['birthdate'],
      photo_candidat : jsonPhotoCandidat(json),
      candidat_election : jsonCandidatElection(json),
      sexe: Sexe.fromJson(
        json['sexe'],
      ),
    );
  }

  static List<PhotoCandidat> jsonPhotoCandidat(json) {
    var list = json['photo_candidat'] as List;
    List<PhotoCandidat> datas = list.map((data) => PhotoCandidat.fromJson(data)).toList();
    return datas;
  }

  static List<CandidatElection> jsonCandidatElection(json) {
    var list = json['candidat_election'] as List;
    List<CandidatElection> datas = list.map((data) => CandidatElection.fromJson(data)).toList();
    return datas;
  }
}

