class CropEntity {
  final String name;
  final String description;

  CropEntity(this.name, [this.description]);
}
