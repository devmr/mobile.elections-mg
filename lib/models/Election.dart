
import 'package:flutter/cupertino.dart';

class Election {

  int id;
  String name;
  bool encours;
  String slug;
  String officialdate;
  bool results;
  bool stats;
  int partiCount;
  int candidatCount;

  Election({
    this.id,
    this.name,
    this.encours,
    this.slug,
    this.officialdate,
    this.results,
    this.stats,
    this.partiCount,
    this.candidatCount
  });

  factory Election.fromJson(Map<String, dynamic> json) {
    return Election(
      id: json['id'],
      name: json['name'],
      encours: json['encours'],
      slug: json['slug'],
      officialdate: json['officialdate'],
      results: json['results'],
      stats: json['stats'],
      partiCount: json['partiCount'],
      candidatCount: json['candidatCount']
    );
  }
}

