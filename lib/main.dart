
import 'package:electionsflutter/pages/LoginPage.dart';
import 'package:electionsflutter/services/api.dart';
import 'package:flutter/material.dart';
import 'package:electionsflutter/pages/homePage.dart';

main() async {
  Widget _defaultHome = new MyHomePage(title: 'Elections-mg');

  // Get result of the login function.
  bool _result = await Api.login();
  if (_result) {
    _defaultHome = new LoginPage();
  }

  // Run app!
  runApp(new MaterialApp(
    title: 'Flutter Demo',
    theme: ThemeData(
      primarySwatch: Colors.red,
    ),
    debugShowCheckedModeBanner: false,
    home: _defaultHome,
    routes: <String, WidgetBuilder>{
      // Set routes for using the Navigator.
      '/home': (BuildContext context) => new MyHomePage(title: 'Elections-mg'),
      '/login': (BuildContext context) => new LoginPage()
    },
  ));
}


